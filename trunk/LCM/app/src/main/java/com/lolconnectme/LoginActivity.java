package com.lolconnectme;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.viewpagerindicator.CirclePageIndicator;


public class LoginActivity extends PlusBaseActivity {
    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;

    FragmentPagerAdapter adapterViewPager;
    private SignInButton btnSignIn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnSignIn = (SignInButton) findViewById(R.id.btn_sign_in);
        Toolbar actionBar = (Toolbar) findViewById(R.id.custom_toolbar);
        actionBar.setTitle("Login");
        setSupportActionBar(actionBar);
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        adapterViewPager = new MyPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapterViewPager);

        //Bind the title indicator to the adapter
        CirclePageIndicator indicator = (CirclePageIndicator)findViewById(R.id.indicator);
        indicator.setViewPager(viewPager);
        // Button click listeners
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInWithGplus();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();
        if(id == R.id.action_settings)
            signOutFromGplus();
        return true;
    }

    @Override
    public void onConnected(Bundle arg0) {
        super.onConnected(arg0);
        updateHomeUI(true);
    }

    @Override
    public void onResume(){
        super.onResume();
        if(getGoogleApiClient().isConnected())
            updateHomeUI(true);
    }


    public class MyPagerAdapter extends FragmentPagerAdapter {
        private int NUM_ITEMS = 3;
        String[] screenSlideTexts;
        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
            screenSlideTexts = getResources().getStringArray(R.array.screen_slide_pages);
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return ScreenSlidePageFragment.newInstance(null, screenSlideTexts[0]);
                case 1:
                    return ScreenSlidePageFragment.newInstance(null, screenSlideTexts[1]);
                case 2:
                    return ScreenSlidePageFragment.newInstance(null, screenSlideTexts[2]);
                default:
                    return null;
            }
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            return "Page " + position;
        }
    }
}

