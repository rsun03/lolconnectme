package com.lolconnectme;

import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by robert.sun on 6/16/2015.
 */
public class GoogleApiClientSingleton {
    private static final String TAG = "MyGoogleApiClientSingleton";
    private static GoogleApiClientSingleton instance = null;

    private static GoogleApiClient mGoogleApiClient = null;

    protected GoogleApiClientSingleton() {

    }

    public static GoogleApiClientSingleton getInstance(GoogleApiClient aGoogleApiClient) {
        if(instance == null) {
            instance = new GoogleApiClientSingleton();
            if (mGoogleApiClient == null)
                mGoogleApiClient = aGoogleApiClient;
        }
        return instance;
    }

    public static GoogleApiClient getGoogleApiClient(){
        return mGoogleApiClient;
    }
}
