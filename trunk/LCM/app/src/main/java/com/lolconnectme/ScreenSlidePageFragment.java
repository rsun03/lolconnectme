package com.lolconnectme;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by robert.sun on 6/16/2015.
 */
public class ScreenSlidePageFragment extends Fragment {


    private static final String TEXT = "text";
    private static final String IMAGE_URL = "img";
    private String mText, mImageUrl;
    private Activity context;
    public ScreenSlidePageFragment(){
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ArticlePostFragment.
     */
    public static ScreenSlidePageFragment newInstance(String url, String text) {
        ScreenSlidePageFragment fragment = new ScreenSlidePageFragment();
        Bundle args = new Bundle();
        args.putString(IMAGE_URL, url);
        args.putString(TEXT, text);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        context = getActivity();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //((AppCompatActivity) context).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getArguments() != null) {
            mImageUrl = getArguments().getString(IMAGE_URL);
            mText = getArguments().getString(TEXT);
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_login_screen_slide, container, false);
        return rootView;
    }

    public void onViewCreated(View view, Bundle savedInstanceState){
        ImageView image = (ImageView) view.findViewById(R.id.slide_img);
        TextView textView = (TextView) view.findViewById(R.id.slide_text);
        textView.setText(mText);
        if(mImageUrl != null)
            Picasso.with(context).load(mImageUrl).into(image);
    }

}
